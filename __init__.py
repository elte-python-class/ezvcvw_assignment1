class Protein:

     def __init__(self, protein):
        
        # Variables to make the process easier

        self.protein = protein
        self.lines = self.protein.split("\n")
        self.varLine = self.lines[0]
        
        # Variables

        self.os = self.varLine.split("OS=")[1].split(" OX")[0]
        self.ox = int(self.varLine.split("OX=")[1].split(" GN")[0])
        self.gn = self.varLine.split("GN=")[1].split(" PE")[0]
        self.pe = int(self.varLine.split("PE=")[1].split(" SV")[0])
        self.sv = int(self.varLine.split("SV=")[1])
        
        # Make of name and id varable

        self.nameAndId = self.varLine.split(" OS")[0].split("|")
        self.name = self.nameAndId[2]
        self.id = self.nameAndId[1]

        # Getting aminoacid lines together

        self.amacOfProt = ""

        for j,k in enumerate(self.lines):
            if j > 0:
                self.amacOfProt += k

        # Getting the number of aminoacids in the protein

        self.size = len(self.amacOfProt)

        # Dictionary which gives back the number of individual aminoacids in the protein
        
        self.aminoacidcounts = {
                    "Y" : self.amacOfProt.count("Y"),
                    "W" : self.amacOfProt.count("W"),
                    "G" : self.amacOfProt.count("G"),
                    "H" : self.amacOfProt.count("H"),
                    "A" : self.amacOfProt.count("A"),
                    "E" : self.amacOfProt.count("E"),
                    "C" : self.amacOfProt.count("C"),
                    "F" : self.amacOfProt.count("F"),
                    "I" : self.amacOfProt.count("I"),
                    "K" : self.amacOfProt.count("K"),
                    "L" : self.amacOfProt.count("L"),
                    "M" : self.amacOfProt.count("M"),
                    "N" : self.amacOfProt.count("N"),
                    "P" : self.amacOfProt.count("P"),
                    "Q" : self.amacOfProt.count("Q"),
                    "R" : self.amacOfProt.count("R"),
                    "S" : self.amacOfProt.count("S"),
                    "T" : self.amacOfProt.count("T"),
                    "U" : self.amacOfProt.count("U"),
                    "V" : self.amacOfProt.count("V")
                }
     def __repr__(self):
        return str(self.name + " id: " + self.id)

     def __eq__(self, other):
        return ((self.size) == (other.size))

     def __lt__(self, other):
        return ((self.size) < (other.size))

     def __gt__(self, other):
        return ((self.size) > (other.size))

     def __ge__(self, other):
        return ((self.size) >= (other.size))

     def __le__(self, other):
        return ((self.size) <= (other.size))

     def __ne__(self, other):
        return ((self.size) != (other.size))



def load_fasta(newFile):
    
    arg = open(newFile, "r")
    argument = arg.read()
    arg.close()

    protList = []
    argument = argument.split(">")

    for h,i in enumerate(argument):
        if h > 0:
            protList.append(Protein(i))

    return protList


def sort_proteins_pe(prot):

    prot = sorted(prot, key = lambda x: x.pe, reverse = True)

    return prot


def sort_proteins_aa(prot, letter):

    prot = sorted(prot, key = lambda x: x.amacOfProt.count(letter), reverse = True)

    return prot


def find_protein_with_motif(prot, motif):
    
    haveMotif = []

    for m in prot:
        if motif in m.amacOfProt:
            haveMotif.append(m)

    return haveMotif
